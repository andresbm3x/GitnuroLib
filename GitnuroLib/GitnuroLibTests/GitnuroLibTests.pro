#-------------------------------------------------
#
# Project created by QtCreator 2016-12-08T15:44:08
#
#-------------------------------------------------

QT       += testlib

QT       -= gui

TARGET = tst_gitnurolibteststest
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += tst_gitnurolibteststest.cpp \
    src/Checkout.cpp \
    src/Clone.cpp \
    src/Diff.cpp \
    src/Fetch.cpp \
    src/Init.cpp \
    src/Push.cpp \
    src/Rebase.cpp \
    src/Repository.cpp \
    src/Revision.cpp \
    src/TestHelpers.cpp
DEFINES += SRCDIR=\\\"$$PWD/\\\"

HEADERS += \
    src/TestHelpers.h
