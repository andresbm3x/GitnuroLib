#ifndef GITNUROLIB_H
#define GITNUROLIB_H

#define GITNUROLIB_VERSION "0.24.4"
#define GITNUROLIB_VER_MAJOR 0
#define GITNUROLIB_VER_MINOR 24
#define GITNUROLIB_VER_REVISION 4

#define GITNUROLIB_SOVERSION 1

#include "gitnurolib_global.h"
#include "src/qgitblob.h"
#include "src/qgitcheckoutoptions.h"
#include "src/qgitcherrypickoptions.h"
#include "src/qgitcommit.h"
#include "src/qgitconfig.h"
#include "src/qgitcredentials.h"
#include "src/qgitdatabase.h"
#include "src/qgitdatabasebackend.h"
#include "src/qgitdiff.h"
#include "src/qgitdiffdelta.h"
#include "src/qgitdifffile.h"
#include "src/qgitexception.h"
#include "src/qgitglobal.h"
#include "src/qgitindex.h"
#include "src/qgitindexentry.h"
#include "src/qgitindexmodel.h"
#include "src/qgitmergeoptions.h"
#include "src/qgitobject.h"
#include "src/qgitoid.h"
#include "src/qgitref.h"
#include "src/qgitremote.h"
#include "src/qgitrepository.h"
#include "src/qgitrevwalk.h"
#include "src/qgitsignature.h"
#include "src/qgitstatus.h"
#include "src/qgitstatusentry.h"
#include "src/qgitstatuslist.h"
#include "src/qgitstatusoptions.h"
#include "src/qgittag.h"
#include "src/qgittree.h"
#include "src/qgittreeentry.h"

#endif // GITNUROLIB_H
