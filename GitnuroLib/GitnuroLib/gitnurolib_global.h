#ifndef GITNUROLIB_LIB_CONFIG_H
#define GITNUROLIB_LIB_CONFIG_H

#include <QtCore/QtGlobal>

/** @defgroup GitnuroLib The Qt Library for Git revision control featuring libgit2
 * Qt wrapper classes for the LibGit2 library.
 */

#ifndef GITNUROLIB_EXPORT
#if defined(MAKE_GITNUROLIB_LIB)
#define GITNUROLIB_EXPORT Q_DECL_EXPORT
#else
#define GITNUROLIB_EXPORT Q_DECL_IMPORT
#endif
#endif

#ifndef GITNUROLIB_EXPORT_DEPRECATED
#define GITNUROLIB_EXPORT_DEPRECATED Q_DECL_DEPRECATED GITNUROLIB_EXPORT
#endif


#ifndef GITNUROLIB_FUNC_NAME
#define GITNUROLIB_FUNC_NAME __func__
#endif

#endif // GITNUROLIB_LIB_CONFIG_H
