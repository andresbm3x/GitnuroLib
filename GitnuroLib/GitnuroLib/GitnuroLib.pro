#-------------------------------------------------
#
# Project created by QtCreator 2016-12-08T15:43:45
#
#-------------------------------------------------

QT       -= gui

TARGET = GitnuroLib
TEMPLATE = lib

DEFINES += GITNUROLIB_LIBRARY

SOURCES += \
    src/private/annotatedcommit.cpp \
    src/private/buffer.cpp \
    src/private/pathcodec.cpp \
    src/private/remotecallbacks.cpp \
    src/private/strarray.cpp \
    src/qgitblob.cpp \
    src/qgitcheckoutoptions.cpp \
    src/qgitcherrypickoptions.cpp \
    src/qgitcommit.cpp \
    src/qgitconfig.cpp \
    src/qgitcredentials.cpp \
    src/qgitdatabase.cpp \
    src/qgitdatabasebackend.cpp \
    src/qgitdiff.cpp \
    src/qgitdiffdelta.cpp \
    src/qgitdifffile.cpp \
    src/qgitexception.cpp \
    src/qgitglobal.cpp \
    src/qgitindex.cpp \
    src/qgitindexentry.cpp \
    src/qgitindexmodel.cpp \
    src/qgitmergeoptions.cpp \
    src/qgitobject.cpp \
    src/qgitoid.cpp \
    src/qgitrebase.cpp \
    src/qgitrebaseoptions.cpp \
    src/qgitref.cpp \
    src/qgitremote.cpp \
    src/qgitrepository.cpp \
    src/qgitrevwalk.cpp \
    src/qgitsignature.cpp \
    src/qgitstatus.cpp \
    src/qgitstatusentry.cpp \
    src/qgitstatuslist.cpp \
    src/qgitstatusoptions.cpp \
    src/qgittag.cpp \
    src/qgittree.cpp \
    src/qgittreeentry.cpp

HEADERS += gitnurolib.h\
        gitnurolib_global.h \
    src/private/annotatedcommit.h \
    src/private/buffer.h \
    src/private/pathcodec.h \
    src/private/remotecallbacks.h \
    src/private/strarray.h \
    src/credentials_p.h \
    src/qgitblob.h \
    src/qgitcheckoutoptions.h \
    src/qgitcherrypickoptions.h \
    src/qgitcommit.h \
    src/qgitconfig.h \
    src/qgitcredentials.h \
    src/qgitdatabase.h \
    src/qgitdatabasebackend.h \
    src/qgitdiff.h \
    src/qgitdiffdelta.h \
    src/qgitdifffile.h \
    src/qgitexception.h \
    src/qgitglobal.h \
    src/qgitindex.h \
    src/qgitindexentry.h \
    src/qgitindexmodel.h \
    src/qgitmergeoptions.h \
    src/qgitobject.h \
    src/qgitoid.h \
    src/qgitrebase.h \
    src/qgitrebaseoptions.h \
    src/qgitref.h \
    src/qgitremote.h \
    src/qgitrepository.h \
    src/qgitrevwalk.h \
    src/qgitsignature.h \
    src/qgitstatus.h \
    src/qgitstatusentry.h \
    src/qgitstatuslist.h \
    src/qgitstatusoptions.h \
    src/qgittag.h \
    src/qgittree.h \
    src/qgittreeentry.h

unix {
    target.path = /usr/lib
    INSTALLS += target
}

DISTFILES +=

win32:CONFIG(release, debug|release): LIBS += -L$$PWD/../libgit/build/release/ -lgit2
else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/../libgit/build/debug/ -lgit2
else:unix: LIBS += -L$$PWD/../libgit/build/ -lgit2

INCLUDEPATH += $$PWD/../libgit/include
DEPENDPATH += $$PWD/../libgit/include
